accountsservice
acpi
acpid
alsa-firmware
alsa-utils
android-tools
android-udev
autoconf
automake
avahi
b43-fwcutter
bash
binutils
bison
bluez-utils
btrfs-progs
bzip2
cantarell-fonts
chromium
clamav
composer
compton
coreutils
cpupower
crda
cronie
cryptsetup
cups
cups-pdf
cups-pk-helper
davfs2
device-mapper
dhclient
dhcpcd
diffutils
dmidecode
dmraid
dnsmasq
dosfstools
dunst
e2fsprogs
eclipse-java
ecryptfs-utils
efibootmgr
exfat-utils
f2fs-tools
fakeroot
feh
ffmpeg
ffmpegthumbnailer
file
filesystem
filezilla
findutils
firefox
flex
freetype2
gawk
gcc
gcc-libs
gconf
gettext
ghostscript
gimp
git
glibc
gnome-icon-theme
gnome-keyring
gnome-themes-extra
go
go-tools
grep
grub
gsfonts
gst-libav
gst-plugins-bad
gst-plugins-base
gst-plugins-good
gst-plugins-ugly
gtk3
gtkmm
gufw
gvfs
gvfs-afc
gvfs-gphoto2
gvfs-mtp
gvfs-nfs
gvfs-smb
gzip
haveged
hplip
htop
inetutils
intel-ucode
iproute2
iptables
iputils
ipw2100-fw
ipw2200-fw
jdk8-openjdk
jfsutils
jre8-openjdk
jre8-openjdk-headless
keepassxc
less
lib32-flex
lib32-mesa-demos
lib32-nvidia-utils
libdvdcss
libgsf
libmpdclient
libopenraw
libreoffice-still
libtool
licenses
linux-firmware
linux-headers
logrotate
lsb-release
lvm2
m4
make
man-db
man-pages
mdadm
meld
memtest86+
mesa-demos
mlocate
mobile-broadband-provider-info
modemmanager
mosh
mtools
mtpfs
nano
neofetch
net-tools
network-manager-applet
networkmanager
networkmanager-openconnect
networkmanager-openvpn
networkmanager-pptp
networkmanager-vpnc
nfs-utils
nmap
nss-mdns
ntfs-3g
ntp
numlockx
nvidia
nvidia-settings
nvidia-utils
openresolv
openssh
os-prober
p7zip
pacman
patch
patchutils
pavucontrol
pciutils
pcmanfm
perl
perl-file-mimeinfo
php-cgi
physlock
pkgconf
poppler-data
poppler-glib
powerline
powertop
procps-ng
psmisc
pulseaudio-alsa
pulseaudio-bluetooth
pulseaudio-zeroconf
pyqt5-common
python-pillow
python-pip
python-pyqt5
python-pywal
python-reportlab
python-virtualenv
qpdfview
reiserfsprogs
rofi
rsync
s-nail
samba
screenfetch
scrot
sed
shadow
splix
steam
subversion
sudo
sxhkd
sysfsutils
syslinux
systemd-sysvcompat
tar
terminus-font
texinfo
thunderbird
tilix
tk
tlp
ttf-bitstream-vera
ttf-droid
ttf-font-awesome
ttf-inconsolata
ttf-indic-otf
ttf-liberation
ttf-roboto
udiskie
udisks2
unace
unrar
unzip
usb_modeswitch
usbutils
util-linux
vi
virtualbox
vlc
wget
which
wine
wireguard-tools
wireless_tools
wireshark-gtk
wmname
wpa_supplicant
xclip
xcursor-simpleandsoft
xcursor-vanilla-dmz-aa
xdg-user-dirs
xdg-utils
xdotool
xf86-input-elographics
xf86-input-evdev
xf86-input-keyboard
xf86-input-libinput
xf86-input-mouse
xf86-input-void
xfsprogs
xorg-server
xorg-twm
xorg-xinit
xorg-xkill
xorg-xprop
xorg-xsetroot
xss-lock
xterm
yarn
zip
zsh
